# Yolov5 container

For this repository we are using the code from https://github.com/ultralytics/yolov5 where we changed the dockerfile so we can train our model inside a docker container

## Usage

1. Build docker image

	<code>
	docker build -t yolov5_image
	</code>

2. Start training  

	<code>
	docker run -it --rm --gpus all --ipc=host --ulimit memlock=-1 --ulimit stack=67108864 --name yolo5_train -v /home/david/workspace/YOLO5/Data_Sow_Dataset:/usr/src/Data_Sow_Dataset -v /home/david/workspace/YOLO5/yolov5/runs:/usr/src/app/runs yolov5_image python3 train.py --img 416 --cfg yolov5s.yaml --hyp hyp.scratch.yaml --batch 32 --epochs 100 --data data_sow.yaml --weights yolov5s.pt --workers 24 --name yolo5_cevni
	</code>


## Further commands:

### Start container with opened bash

	<code>
	docker run -it --rm --gpus all --ipc=host --ulimit memlock=-1 --ulimit stack=67108864 --name yolo5_train -v <PATH_TO_DATASET>:/usr/src/Data_Sow_Dataset yolov5_image /bin/bash
	</code>